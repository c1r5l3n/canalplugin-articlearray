# QR-generator plugin for flood/canal

## Usage

This block ist for a Flood\Canal project.
It renders a div with a link-list of all current articles.

## How to integrate it to your canal-project

To add an article-link-list, you have to add it to `_content.php`

Add the following code to the function:

    $content->addBlock('pdf_creator', __DIR__ . /vendor/canalplugin/pdf/src');

No further config or setup required.
          
## Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

## Copyright

Carolin Holat



